## Sheet for the implementation of the Wu list decoding algorithm,
## both for Reed--Solomon codes and binary Goppa codes
##
## Written by Johan S. R. Nielsen, 2013. Converted to sheet and updated in 2014
##
## The algorithm for decoding binary Goppa codes, as well as the
## description for the RS decoder were proposed in:
##   On Rational Interpolation-Based List-Decoding and List-Decoding Binary Goppa Codes
##   Beelen, Peter, Tom Hoholdt, Johan S. R. Nielsen, and Yingquan Wu
##   IEEE Transactions on Information Theory 59, no. 6 (June 2013): 3269–81. doi:10.1109/TIT.2013.2243800.
##
## RS list decoding using rational interpolation was originally proposed in
##   New List Decoding Algorithms for Reed-Solomon and BCH Codes
##   Wu, Yingquan
##   IEEE Transactions on Information Theory 54, no. 8 (2008): 3611–30.
##
## This implementation is in Sage (v. 5.13, http://sagemath.org). It depends on
## Codinglib by Johan S. R. Nielsen (http://jsrn.dk/codinglib). This
## sheet was tested and verified using the commit  e6b0a3a  of codinglib.
##
## Please read the README of the code repository for a general description of
## the sheet format and usage instructions. Be aware that this is a
## simple research implementation. It is not well documented and
## probably not fully generic.

## LICENSE INFORMATION:
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##


### Implementation of the EEA and Groebner basis-like algorithms for the resulting basis
def ea_groebner_basis(p, q, eaRes):
    """Calculate the module groebner basis from the result of the EA on p and
    q"""
    P.<y> = p.parent()[]
    (vc, vl, uc, ul) = (eaRes[0][2], eaRes[1][2], eaRes[0][1], eaRes[1][1])
    Q1 = ul*p - vl*(y - q)
    Q2 = uc*p - vc*(y - q)
    return Q1, Q2

def module_groebner_zero(basis, weights=None, order="(lp,C)"):
    """Calculates a Groebner basis of the module spanned by the list of
    basis vectors given, using the top-ordering with last position biggest.
    E.g. for two-dimensional, (0,x) < (x^2, 0) < (0, x^2).
    If weights are given, it should be a list of integers of length
    ``|basis[0]|''. The polynomial at position of each basis will then be
    multiplied with ``x^weights[i]'' before and after computation.
    Calls Singular directly. NOTE: Reboots the Singular interface!"""
    n = len(basis)
    l = len(basis[0])
    R = basis[0][0].base_ring()
    P = basis[0][0].parent()
    x = P.gen()
    if weights:
        basis = [ [ x^weights[i]*b[i] for i in range(0,l) ] for b in basis ]
    singular.quit()
    #Make sure that the correct ring is initialise
    singR = singular.ring(R.cardinality(), R.variable_name(), order)
    for i in range(0, n):
        singular.eval("vector v%s=%s;" % (i, str(list(basis[i]))) )
    basis_str = ",".join("v%s" % i for i in range(0,n))
    singular.eval("module m=%s;" % (basis_str,))
    T = singular("std(m);")
    reduced = [ vector(T[j,i].sage() for j in range(1, l+1) )
                    for i in range(1, n+1) ]
    if weights:
        reduced = [ [ P(b[i]/x^weights[i]) for i in range(0,l) ]
                        for b in reduced ]
    return reduced

def reduce_module_element_in_groebner(e, T, posweight):
    """T is a groebner basis of a module over a univariate polynomial ring,
    represented as vectors with elements being such polynomials, using the
    top-ordering with last position biggest, and where the i'th entry has
    posweight added to its degree. Then reduce the element e in this
    basis"""
    P = e[0].parent()
    R = e[0].base_ring()
    PD.<x,y> = PolynomialRing(R, 2, order=TermOrder("wdeglex",(1,posweight)))
    PP.<yy> = P['y']
    l = len(e)
    def toPol(e):
        return sum(y^i*e[i](x) for i in range(0, l))
    def fromPol(ep):
        cs = PP(ep).coeffs()
        return vector(cs + [P(0)]*(l-len(cs))) #make sure its length is l
    Tp = [ toPol(t) for t in T ]
    return fromPol(toPol(e).reduce(Tp))

def module_divide(f, Q1, Q2, yweight):
    """Specialised bivariate division algorithm, under the ordering
    wdlex(1,yweight) with y<x in the lex ordering. f, Q1, Q2 must be members of
    F[x][y]. Returns c1, c2, s such that f = c1*Q1 + c2*Q2 + s and no term in s
    is divisible by lt(Q1) or lt(Q2).
    """
    Px  = Q1.base_ring()
    Pxy = Q1.parent()
    #We represent the bivariate polynomials as 2-tuples: zeroth index is y^0 and first is y^1.
    def toTuple(p):
        pl = p.list()
        return (p[0], p[1])
    def toPoly(p):
        return p[0] + p[1]*Pxy.gen()
    def tAdd(t1, t2):
        return (t1[0]+t2[0], t1[1]+t2[1])
    def tScale(t, scale):
        return (t[0]*scale, t[1]*scale)
    f, Q1, Q2 = toTuple(f) , toTuple(Q1) , toTuple(Q2) # From now on f, Q1 and Q2 are tuples of polys in x
    def degree(pt):
        return (pt[0].degree(), pt[1].degree())
    def lt(pt):
        #A largest term is represented as (i, d) where i is the degree of y, d the degree of x.
        deg = degree(pt)
        if deg[0] >= deg[1]+yweight: # Using >= (instead of >) ensures y<x in the lex ordering
            return (0, deg[0])
        else:
            return (1, deg[1])
    ltQ1 = lt(Q1)
    ltQ2 = lt(Q2)
    r = f       # current remainder
    s = Px(0),Px(0)   # collected remainder (undivisible)
    c1,c2 = Px(0),Px(0) # coefficients to Q1 and Q2 (in F[x])
    def divide(r, ltr, Q, ltQ):
        if ltr[0] == ltQ[0] and ltr[1] >= ltQ[1]:
            c = x^(ltr[1]-ltQ[1]) * r[ltr[0]].leading_coefficient() / Q[ltQ[0]].leading_coefficient()
            r = tAdd(r, tScale(Q,-c))
            return (r, c)
        return (r, 0)
    def sliceLt(r, ltr):
        sv = x^ltr[1] * r[ltr[0]].leading_coefficient() 
        sn = (sv, 0) if ltr[0] == 0 else (0, sv)
        r = tAdd(r, tScale(sn, -1))
        return (sn, r)
    while r[0] != 0 or r[1] != 0:
        ltr = lt(r)
        #print ltr, ltQ1, ltQ2
        (r, c) = divide(r, ltr, Q1, ltQ1)
        if c!=0:
            c1 = c1 + c
        else:
            (r, c) = divide(r, ltr, Q2, ltQ2)
            if c!=0:
                c2 = c2 + c
            else:
                #print "Slicing"
                (sn, r) = sliceLt(r, ltr)
                s = tAdd(s, sn)
    return (c1, c2, toPoly(s))





# ###############
### The Wu list decoding algorithm for GRS codes using EA (column multipliers = 1)
# ###############
# Follows the Goppa article point-wise

# First a helper function
def rs_attemptDecoding(C, errLoc, r, S, g, maxDeg=None):
    """Attempt to decode the received word r in the code C using the direct error locator errLoc.
    S must be the syndrome associated with the direct error locator.
    g is the modulus, i.e. x^2t divided by the greatest common divisor with the original syndrome."""
    if maxDeg and errLoc.degree() > maxDeg:
        print("attemptDecoding failed due to excess degree")
        return None
    (n,k,alphas) = C.n, C.k, C.alphas
    t = int((n-k)/2)
    roots = [ i for i in range(0, n) if errLoc(alphas[i]).is_zero() ]
    if not len(roots) == errLoc.degree():
        print("attemptDecoding failed due to incorrect number of roots")
        return None
    errEval = (errLoc/errLoc.leading_coefficient()*S).mod(g)
    c = [ ri for ri in r ] # copy r
    for i in roots:
        e = errEval(alphas[i]) / C.error_evaluator_factor(i, roots, direct=True)
        c[i] = c[i] - e
    # OBS: Has not been checked whether it is a codeword yet.
    return c

def wu_grs_decode(C, tau, r, x, debug=False):
    (n,k,d,alphas) = C.n, C.k, C.d, C.alphas
    time_ping()
    ### POINT 1)
    S = list2poly(C.syndrome(r, direct=True), x)
    if S.is_zero():
        print "No errors"
        return r
    if debug:
        print "Syndrome", S
    g = x^(n-k)
    time_ping(mes="Point 1")
    ### POINT 2)
    eaRes = ea(g, S, dt=0)
    h1, h2 = -eaRes[1][2], -eaRes[0][2]
    time_ping(mes="Point 2")
    ### POINT 3)
    c = rs_attemptDecoding(C, h2, r, S, g, maxDeg=int((n-k)/2))
    if c and C.iscodeword(c):
        time_ping(mes="Point 3")
        if debug:
            print "errloc: ", h2
        print "Minimum-distance decoding"
        return c
    time_ping(mes="Point 3")
    ### POINT 4)
    w1 = tau - d + h2.degree()
    w2 = tau - h2.degree()
    (s,l) = wu_params_rs(n,k,tau)
    points = [ (a, h1(a), h2(a)) for a in alphas ]
    Q = rat_construct_Q(points, tau , (s, l), (w2, w1))
    time_ping(mes="Point 4")
    ### DEBUG BAIL
    if debug:
        return points, Q, eaRes, (s,l)
    ### POINT 5)
    factors = rat_find_factors(Q, w1, w2)
    if not factors:
        print Q
        print "No valid factors of interpolation polynomial"
        return None
    time_ping(mes="Point 5")
    ### POINT 6)
    codewords = []
    for (f1, f2) in factors:
        Lambda = f1*h1 + f2*h2
        if debug:
            print f1, f2, h1, h2, Labda
        c = rs_attemptDecoding(C, Lambda, r, S, g)
        if not c is None:
            codewords.append(c)
    if not codewords:
        print "Decoding FAILED: No valid error locators as factors of Q"
        return None
    time_ping(mes="Point 6")
    ### POINT 7)
    valid_codewords = [ c for c in codewords if C.iscodeword(c) ]
    if valid_codewords:
        print "Decoding SUCCESS! Found %d codewords" % len(valid_codewords)
    else:
        print "Decoding FAILED"
    time_ping(done=True)
    return valid_codewords



    
### Test the GRS decoder
# Set up an example code
q = 256
F.<a> = GF(q,'a')
PF.<x> = F[]
n = 254
k = 50
alphas = F.list()[1:n+1] # Assumes that the zeroth elem is 0
#print alphas
C = GRS(F,n,k,alphas)
taurange = list_decoding_range(n,n-k+1)
tau = taurange[0] + 18
print "Code is %s" % C
print "Min-dist decoding is %d. Maximal list decoding is %d" % (int(C.d/2), taurange[1])
print "Decoding range is %s, which is %s more than min-dist" % (tau, tau-int(C.d/2))
decStats = wu_stats_rs(n,k,tau)
print "Ratinter-stats: ", decStats
if decStats['matrixsize'] > 10^7:
    print "WARNING: Very large rational interpolation system to solve"


### Construct an information word, encode and add a MINIMUM DISTANCE correctable amount of errors
info = random_vector(F,k)
#print "Info: ", info
c = C.encode(info)
#print "Codeword: ", c
nerrs = randint(1,int((n-k)/2))
print "Adding %d errors." % nerrs
e = random_error(n, F, nerrs)
#print "Errors: %s" % e
r = [ c[i] + e[i] for i in range(0,n) ]

### 
rcorr = wu_grs_decode(C, tau, r, x)
if all([ c[i] == rcorr[i] for i in range(0,n) ]):
    print "Success"
else:
    print "Failure:"
    print "c: ", c
    print "r: ", r
    print "c':", rcorr


### Construct an information word, encode and add a LIST correctable amount of errors
print "Code is %s" % C
info = random_vector(k, F)
#print "Info: ", info
c = C.encode(info)
nerrs = randint(ligt((n-k)/2), tau)
print "Adding %d errors." % nerrs
e = random_error(n, F, nerrs)
#print "Errors: %s" % e
r = [ c[i] + e[i] for i in range(0,n) ]

###
rcorrlist = wu_grs_decode(C, tau, r, x)
if rcorrlist and all( all( c[i] == rcorr[i] for i in range(0,n) ) for rcorr in rcorrlist):
    print "Success"
else:
    print "Failure:"
    print "c: ", c
    print "r: ", r
    print "c':", rcorrlist


#
### Other tests for the GRS decoder
# By stopping the algorithm right after constructing Q and letting it
# return Q, check that the points indeed are zeroes of Q

def is_zero_of_mult(Q, (x0,y0,z0), s):
    (xx,yy,zz) = Q.parent().gens()
    if not z0.is_zero():
        Qrem = Q(xx+x0,yy+(y0/z0),1)
    else:
        Qrem = Q(xx+x0,1,yy)
    for i in range(0, s):
        for j in range(0, s-i):
                #print "trying %s,%s at %s,%s,%s" % (i,j, x0,y0,z0)
                if not Qrem.coefficient({xx: i, yy: j, zz: 0}).is_zero():
                    return False
    return True

###
(points, Q, eaRes, (s,l)) = wu_grs_decode(C, tau, r, x, debug=True)
for (x0,y0,z0) in points:
    assert(Q(x0,y0,z0) == 0)
    assert(is_zero_of_mult(Q, (x0,y0,z0), s))
print "Success"



# ###########
### Binary Goppa codes list decoder
# ###########

# Helper functions for computing square root in the polynomial quotient ring
_poly_quot_T = None
def poly_quot_sqrt_prepare(FQuot):
    global _poly_quot_T
    w = FQuot.degree()
    x = FQuot.gen()
    squares = [ (x^(2*i)).list() for i in range(0,w) ]
    M = matrix(w,w, lambda i,j: squares[j][i] )
    _poly_quot_T = (FQuot, M.inverse())

def poly_quot_sqrt(R):
    """Calculate the sqrt of the polynomial R according to the prepared polynomial quotient ring"""
    FQuot = _poly_quot_T[0]
    w = FQuot.degree()
    x = FQuot.gen()
    RSqrtVec = vector([ a for a in R.list()])
    SqrtVec = _poly_quot_T[1]*RSqrtVec
    return sum([ SqrtVec[i].sqrt()*x^i for i in range(0, w) ])


# Decoder helper function
def goppa_attemptDecoding(C, errLoc, r, maxdeg=None):
    """Attempt to decode the received word r in the binary Goppa code C using the direct
    error locator errLoc."""
    if maxdeg!=None and errLoc.degree() > maxdeg:
        print("attemptDecoding failed due to exceeded degree")
        return None
    roots = [ i for i in range(0, C.n) if errLoc(C.alphas[i]).is_zero() ]
    if not len(roots) == errLoc.degree():
        print("attemptDecoding failed due to incorrect number of roots")
        return None
    c = [ ri for ri in r ] # copy r
    for i in roots:
        c[i] = c[i] - 1
    if not C.iscodeword(c):
        print("attemptDecoding failed due to non-zero syndrome")
        return None
    return c

# Decoder
def wu_goppa_decode(C, tau, r, debug=None):
    time_ping()
    PF = C.g.parent()
    x = PF.gen()
    FQuot = PF.quotient(g)
    ### POINT 1)
    S = C.syndrome(r)
    Sinv = FQuot(S)^(-1)
    if Sinv.lift() == x:
        print "Sinv is x"
        return None
    #print "Syndrome", S
    T = poly_quot_sqrt(Sinv + FQuot(x))
    T = T.lift()
    if debug==1:
        return S,T
    time_ping(mes="Point 1")
    ### POINT 2)
    eaRes = ea(C.g, T, dt=0)
    h1 = eaRes[1][0]^2 + x*eaRes[1][2]^2
    h2 = eaRes[0][0]^2 + x*eaRes[0][2]^2
    time_ping(mes="Point 2")
    if debug==2:
        return (h1, h2), T, S
    ### POINT 3)
    attempt = 1
    c = None
    if h1.degree() + tau < C.d:
        # We only bother checking h1, h2 if we know that there can be no other codewords besides
        c = goppa_attemptDecoding(C, h1, r, maxdeg=g.degree())
    if c is None:
        attempt = 2
        if h2.degree() + tau < C.d:
            c = goppa_attemptDecoding(C, h2, r, maxdeg=g.degree())
    if not c is None:
        print "Errloc was h%d: %s" % (attempt, h1 if attempt == 1 else h2)
        print "Close minimum-distance decoding. No more decoding necessary"
        return [c]
    time_ping(mes="Point 3")       
    ### POINT 4)
    w1 = tau/2 - C.g.degree() + eaRes[0][2].degree()
    w2 = (tau-1)/2 - eaRes[0][2].degree()
    (s, l) = wu_params_goppa(n,C.g.degree(),tau)
    points = [ (a, h1(a).sqrt(), h2(a).sqrt()) for a in alphas ]
    if debug==3:
        return points, eaRes, T, (s, l), (w1, w2)
    Q = rat_construct_Q(points, tau , (s, l), (w2, w1))
    time_ping(mes="Point 4")
    ### DEBUG BAIL
    if debug==4:
        return points, Q, eaRes, T, (s,l)
    ### POINT 5)
    factors = rat_find_factors(Q, w1, w2)
    if not factors:
        print Q
        print "No valid factors of interpolation polynomial"
        return None
    time_ping(mes="Point 5")
    ### POINT 6)
    codewords = []
    for (f1, f2) in factors:
        Lambda = f1^2*h1 + f2^2*h2
        if debug:
            print f1, f2, h1, h2, Lambda
        c = goppa_attemptDecoding(C, Lambda, r)
        if not c is None:
            codewords.append(c)
    if not codewords:
        print "Decoding FAILED: No valid error locators as factors of Q"
        return None
    time_ping(mes="Point 6")
    ### POINT 7)
    valid_codewords = [ c for c in codewords if C.iscodeword(c) ]
    if valid_codewords:
        print "Decoding SUCCESS! Found %d codewords" % len(valid_codewords)
    else:
        print "Decoding FAILED"
    time_ping(done=True)
    return valid_codewords


#
### Testing the binary Goppa code decoder
# Set up a Goppa code example
m = 7
F = GF(2^m,'alpha')
alpha = F.gen()
PF.<x> = F[]
n = 128
gdeg = 24
g = irreducible_polynomial(PF, gdeg)
print "g(x) = %s" % g
if n>F.cardinality():
    raise Exception("n too large for field")
elif n == F.cardinality():
    alphas = F.list()
else:
    alphas = F.list()[1:n+1] #Assumes that the zeroth elem is 0
assert(len(alphas) == n)
C = GoppaBinary(F, n, g, alphas)
taurange = list_decoding_range(n, 2*gdeg+1, q=2)

print "Code is %s" % C
print "Min-dist decoding is %d. Maximal list decoding is %d" % (int(C.d/2), taurange[1])

tau = taurange[0] + 1
assert(tau <= taurange[1])
poly_quot_sqrt_prepare(PF.quotient(g))

print "Decoding range is %s, which is %s more than min-dist" % (tau, tau-int(C.d/2))
decStats = wu_stats_goppa(n,g.degree(),tau)
print "Ratinter-stats: ", decStats
if decStats['matrixsize'] > 10^7:
    print "WARNING: Very large rational interpolation system to solve"


### Set up example with MINIMUM DISTANCE correctable errors.
# Remember that if this is high, we still need to perform full list
# decoding, as there might be another word within the list decoding radius.
print "Code is a %s" % C
#info = list2poly(random_vector(k, F), x)
#print "Info: ", info
#c = codeword(info, alphas)
c = [ 0 for i in range(0, n) ]
nerrs = randint(1,g.degree())
print "Adding %d errors." % nerrs
e = random_error(n, C.F, nerrs)
print "Errors: %s" % e
r = [ c[i] + e[i] for i in range(0,n) ]

###
rdec = wu_goppa_decode(C, tau, r, debug=4)
if not rdec is None:
    assert(all([ bit.is_zero() for bit in rdec[0] ]))
if rdec is None or len(rdec) != 1:
    print "FAIL!!!"
else:
    print "Success!"


### Construct an information word, encode and add a LIST correctable amount of errors
print "Code is %s" % C
#info = PF(list2poly(random_vector(k, F), x))
#print "Info: ", info
#c = C.encode(info)
c = [ 0 for i in range(0, n) ]
nerrs = randint(ceil(C.d/2), tau)
print "Adding %d errors." % nerrs
e = random_error(n, C.F, nerrs)
#print "Errors: %s" % e
r = [ c[i] + e[i] for i in range(0,n) ]

###
rcorrlist = wu_goppa_decode(C, tau, r)
if rcorrlist and all( all( c[i] == rcorr[i] for i in range(0,n) ) for rcorr in rcorrlist):
    print "Success"
else:
    print "Failure:"
    print "c: ", c
    print "r: ", r
    print "output count: ", len(rcorrlist)
    for i in range(0,len(rcorrlist)):
        print "Word %d:\n\t%s" % (i, rcorrlist[i])



#
### Other functions for the binary Goppa decoder
#

### Finding good Goppa code parameters
nrange = (128, 256)
for n in range(*nrange):
    m = ceil(log(n, 2)) # choose possible m
    gdegrange = (2, min(int((n-1)/m) , int(n/4)) + 1)
    for gdeg in range(*gdegrange):
        taurange = ( gdeg+1 , gilt(1/2*(n-sqrt(n*(n-4*gdeg)))) )
        if taurange[1] > taurange[0]+1:
            print "(n,gdeg,m) = (%s,%s,%s) gives range %s and dimension >= %s" % (n,gdeg,m,taurange,n-gdeg*m)


#
### Example of decoding binary Goppa code that we considered putting in the paper
#
### Firste some printing functions
def latex_pow(base_str, pow):
    if pow==0:
        return ""
    elif pow==1:
        return base_str+" "
    else:
        return "%s^{%s}" % (base_str, pow)
def latex_field_elem(e):
    if e.is_one():
        return ""
    else:
        F = e.parent()
        alpha = F.variable_name()
        pow = int(e.log(F.gen())) #SLOOOOOOOOOOOOW!!!!!!
        # This is idiotic. It's because log_repr returns F.cardinality when it should return 1
        #if pow==F.cardinality():
        #    return alpha+" "
        #else:
        return latex_pow(alpha, pow)
def latex_uni_monom(coeff, base_str, pow):
    # Coeff must be non-zero
    coeff_str = latex_field_elem(coeff)
    mon_str = latex_pow(base_str, pow)
    if coeff_str or mon_str:
        return "%s%s" % (coeff_str, latex_pow(base_str, pow))
    else:
        return "1"
def latex_tri_monom(xPoly, ybase_str, zbase_str, ypow, zpow):
    # xPoly must be non-zero
    xstr = latex_polynomial(xPoly)
    ystr = latex_pow(ybase_str, ypow)
    zstr = latex_pow(zbase_str, zpow)
    if xstr or ystr or zstr:
        if xstr.find(" ") > -1 and (ystr or zstr):
            return "(%s)%s%s" % (xstr, ystr, zstr)
        else:
            return "%s%s%s" % (xstr, ystr, zstr)
    else:
        return "1"
def latex_polynomial(p):
    if len(p.variables()) == 0:
        return latex_field_elem(p.base_ring()(p))
    if len(p.variables()) == 1:
        coeffPows = zip( p.coefficients() , p.exponents() )
        coeffPows.reverse()
        x = p.variable_name()
        return " + ".join([ latex_uni_monom(coeff, x, pow) for (coeff,pow) in coeffPows ])
    elif len(p.variables()) == 3:
        # Assume a trivariate partially homogeneous        
        (x,y,z) = Q.parent().variable_names()
        F = Q.base_ring()
        PFx = F[x]
        PFyz = PFx[y,z]
        pp = PFyz(p)
        coeffPows = zip( pp.coefficients() , pp.exponents() )
        coeffPows.reverse()
        return " + ".join( latex_tri_monom(PFx(coeff), y, z, ypow, zpow) for (coeff,(ypow,zpow)) in coeffPows )
    else:
        print p
        raise Exception("Unsupported no. of variables %s" % (p.variables(),))
        
        
def shortlist_field_elem(e):
    if e.is_zero():
        return "-"
    else:
        F = e.parent()
        pow = int(e.log(F.gen())) #SLOOOOOOOOOOOOW!!!!!!
        return str(pow)
def shortlist_tri_monom(xPoly, ybase_str, zbase_str, ypow, zpow):
    # xPoly must be non-zero
    xstr = shortlist_polynomial(xPoly)
    ystr = latex_pow(ybase_str, ypow)
    zstr = latex_pow(zbase_str, zpow)
    if xstr or ystr or zstr:
        return "%s%s%s" % (xstr, ystr, zstr)
    else:
        return "1"
def shortlist_polynomial(p):
    if len(p.variables()) == 0:
        return "["+shortlist_field_elem(p.base_ring()(p))+"]"
    if len(p.variables()) == 1:
        coeffPows = [ shortlist_field_elem(pe) for pe in p.list() ]
        coeffPows.reverse()
        return "["+ ", ".join(coeffPows) +"]"
    elif len(p.variables()) == 3:
        # Assume a trivariate partially homogeneous        
        (x,y,z) = Q.parent().variable_names()
        F = Q.base_ring()
        PFx = F[x]
        PFyz = PFx[y,z]
        pp = PFyz(p)
        coeffPows = zip( pp.coefficients() , pp.exponents() )
        return " + ".join( shortlist_tri_monom(PFx(coeff), y, z, ypow, zpow) for (coeff,(ypow,zpow)) in coeffPows )
    else:
        print p
        raise Exception("Unsupported no. of variables %s" % (p.variables(),))

### Construct the example
print("---- Construct the example ----")
m = 7
F = GF(2^m,'alpha')
alpha = F.gen()
PF.<x> = F[]
n = 128
g = (alpha^6 + alpha^4 + alpha^2 + alpha)*x^14 + (alpha^6 + alpha^4 + alpha^2 + alpha + 1)*x^13 + (alpha^6 + alpha^3 + alpha^2)*x^12 + (alpha^6 + alpha^4 + alpha)*x^11 + (alpha^4 + alpha^2 + alpha + 1)*x^10 + (alpha^6 + alpha^3 + alpha^2 + alpha + 1)*x^9 + (alpha^6 + alpha^5 + alpha^4 + alpha^3)*x^8 + (alpha^3 + 1)*x^7 + (alpha^4 + alpha^2 + 1)*x^6 + (alpha^6 + alpha^4 + alpha^3 + alpha + 1)*x^5 + (alpha^5 + alpha^4 + alpha^2 + alpha + 1)*x^4 + alpha^5*x^3 + (alpha^6 + alpha^4 + alpha^3)*x^2 + (alpha^4 + alpha^3 + alpha)*x + alpha^5 + alpha^2
assert(g.is_irreducible())
alphas = [0]+[ alpha^i for i in range(0, 2^m-1) ]
assert(len(alphas) == n)
C = GoppaBinary(F, n, g, alphas)
print "Code is %s" % C
print list_decoding_range(n, 2*g.degree()+1, q=2)
tau = 16
poly_quot_sqrt_prepare(PF.quotient(g))
decStats = wu_stats_goppa(n,g.degree(),tau)
print "Ratinter-stats: ", decStats
if decStats['matrixsize'] > 10^7:
    print "WARNING: Very large rational interpolation system to solve"
print latex_polynomial(g)

###
lweight_word = vector(F, [0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0])
assert(lweight_word in C)

### add error, do decoding up until having constructed Q
err = vector(F, [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0,
0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
some_codeword = vector(F, [0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0])
assert(some_codeword in C)
r = err + some_codeword
rstr = "["+ "".join([ str(ri) for ri in r]) +"]"
D = wu_goppa_decode(C, tau, r, debug=2)
(h1,h2) = D[0]
D = wu_goppa_decode(C, tau, r, debug= 4)
Q = D[1]
print "Results:\nr = %s\nh1 = %s\nh2 = %s\nQ = %s"  % (rstr, latex_polynomial(h1), latex_polynomial(h2), latex_polynomial(Q))

### finding the factors
(w1, w2) = (1, 1/2)
factors = rat_find_factors(Q, w1, w2)
print factors

### examine factors
for (fy,fz) in factors:
    print "(%s)y + (%s)z" % (latex_polynomial(fy), latex_polynomial(fz))
    Lambda = fy^2*h1 + fz^2*h2
    assert(len(Lambda.roots()) == Lambda.degree())
    print latex_polynomial(Lambda)

### finish decoding (actually runs entire decoder from beginning)
res = wu_goppa_decode(C, tau, r)
print res


#
### Functions for testing the implementation of the Rational Interpolation procedure
#

### Simple check to see that its ok
assert(rat_monomial_list(5, 3, (1, 2)) == [(0,2,1),(0,3,0),(1,3,0)])

### Test that the monomials found are well-behaved
maxd = randint(1, 1000)
l = randint(1,20)
wy = randint(1, int(maxd/10))
wz = randint(1, int(maxd/10))
mons = rat_monomial_list(maxd, l, (wy, wz))
assert(all([ xp+yp*wy+zp*wz < maxd for (xp,yp,zp) in mons ]))
assert(all([ yp+zp == l for (xp,yp,zp) in mons ]))

### Manual test that the interpolation matrix is ok
F = GF(7)
points = [(F(1),F(1),F(1))]
tau = 3
s, l = 1, 1
w1, w2 = 1, 0

mons = rat_monomial_list(s*tau, l, (w2, w1))
print "monomials %s" % mons
print list ( rat_interpol_matrix(points, s, mons) )
# The above should be the all 1-word as we are only requesting that
# the constant term is zero, and we add 1 to all variables

### Test rat_interpol_matrix for a single point.
# Use a symbolic expression to let Sage automatically calculate the
# correct equations and check that this is the same as ours.
def test_rat_interpol_matrix_point((x0,y0,z0), s, mons):
    # We do this bivariately, according to Trifonov's lemma
    print "\tDefining variables"
    PFqs = F[",".join([ r"q%dq%d" % (i,j) for (i,j,h) in mons ])]
    qs = PFqs.gens()
    PF.<xx,yy> = PFqs["x,y"]
    print "\tCalculating polynomial"
    if not z0.is_zero():
        Q = sum([ qs[i]*xx^mons[i][0]*yy^mons[i][1] for i in range(0,len(mons)) ])
        Qzero = Q(x=xx+x0, y=yy+y0/z0)
    else:
        Q = sum([ qs[i]*xx^mons[i][0]*yy^(l-mons[i][1]) for i in range(0,len(mons)) ])
        Qzero = Q(x=xx+x0, y=yy)
    print "\tExtracting its coefficients"
    polyEqs = set()
    for yp in range(0,s):
        for xp in range(0,s-yp):
            polyEqs.add(Qzero.coefficient([xp,yp]))
    print "\tCalculate using rat_interpol_matrix"
    eqs = list( rat_interpol_matrix([(x0,y0,z0)], s, mons) )
    print "\tWrite its equations symbolically"
    calcEqs = set()
    for eq in eqs:
        calcEqs.add(sum([ qs[i]*eq[i] for i in range(0,len(mons)) ]))
    print "\tCompare"
    assert( polyEqs == calcEqs)

### Infinite loop: Test that the explicit Q matrix is correct (by comparing to symbolic)
while True:
    x0,y0,z0 = F.random_element(), F.random_element(), F.random_element()
    if y0.is_zero() and z0.is_zero():
        continue
    s = randint(1, 5)
    l = randint(s, 6)
    tau = randint(10, 20)
    wy, wz = randint(1,10), randint(1,10)
    mons = rat_monomial_list(s*tau, l, (wy, wz))
    if not mons:
        continue
    print "Attempting (s,l)=(%d,%d), tau=%d, (w1,w2)=(%d,%d), yielding %d monomials" \
                 % (s,l,tau,w1,w2,len(mons))
    print "Point (%d,%d,%d)" % (x0,y0,z0)
    test_rat_interpol_matrix_point((x0,y0,z0), s, mons)
    print "\tSuccess!"
